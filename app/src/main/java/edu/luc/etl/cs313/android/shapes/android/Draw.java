package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;


	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override //was done for us
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0,0, c.getRadius(), paint);
		return null;
	}

	@Override //i think this is good
	public Void onStrokeColor(final StrokeColor c) {
		int p = paint.getColor();
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setColor(p);
		return null;
	}

	@Override //completed do same for onOutline
	public Void onFill(final Fill f) {
		Paint.Style style = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		paint.setStyle(style);
		return null;
	}

	//come back to this
	@Override
	public Void onGroup(final Group g) {
		for(Shape s:g.getShapes()){
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override //done
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override //completed
	public Void onOutline(Outline o) {
		Paint.Style style = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(style);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		final float[] pts = new float[4 * s.getPoints().size()];
		for (int i = 0; i <= s.getPoints().size() - 1; i++) {
			if (i == s.getPoints().size() - 1) {
				pts[4 * i] = s.getPoints().get(i).getX();
				pts[4 * i + 1] = s.getPoints().get(i).getY();
				pts[4 * i + 2] = s.getPoints().get(0).getX();
				pts[4 * i + 3] = s.getPoints().get(0).getY();
			} else {
				pts[4 * i] = s.getPoints().get(i).getX();
				pts[4 * i + 1] = s.getPoints().get(i).getY();
				pts[4 * i + 2] = s.getPoints().get(i + 1).getX();
				pts[4 * i + 3] = s.getPoints().get(i + 1).getY();

			}
		}

		canvas.drawLines(pts, paint);
		return null;
	}

	//public void setV(Visitor<Void> v) {
		//this.v = v;
	}



