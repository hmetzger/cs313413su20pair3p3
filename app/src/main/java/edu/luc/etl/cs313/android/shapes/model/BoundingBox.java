package edu.luc.etl.cs313.android.shapes.model;


/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {
	// TODO entirely your job (except onCircle)

	@Override //all done
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override //done
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
		}


	@Override
	public Location onGroup(final Group g) {
		Location l = g.getShapes().get(0).accept(this);//returns a Location wrapping a Rectangle, which is the BoundingBox for that Shape.
		Rectangle r = (Rectangle) l.getShape();

		int minX = l.getX();
		int minY = l.getY();
		int maxX = minX + r.getWidth();
		int maxY = minY + r.getHeight();

		for(int i =1; i<=g.getShapes().size()-1; i++){
			Location t = g.getShapes().get(i).accept(this);
			Rectangle w = (Rectangle) t.getShape();

			minX = Math.min(minX, t.getX());
			minY = Math.min(minY, t.getY());
			maxX = Math.max(maxX, t.getX() + w.getWidth());
			maxY = Math.max(maxY, t.getY() + w.getHeight());

		}
		Location h = new Location(minX, minY, new Rectangle(maxX - minX, maxY - minY));
		return h;
	}

	@Override
	public Location onLocation(final Location l) {
		Location r = l.getShape().accept(this);
		return new Location(r.getX() + l.getX(), r.getY() + l.getY(), r.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0, 0, r);
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		return onGroup(s);
	}
}
