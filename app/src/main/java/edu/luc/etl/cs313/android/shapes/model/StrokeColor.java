package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Color;

/**
 * A decorator for specifying the stroke (foreground) color for drawing the
 * shape.
 * The StrokeColor decorator indicates the foreground color for drawing its Shape.
 */
public class StrokeColor implements Shape {

	protected final Shape shape;

	protected final int color;

	public StrokeColor(final int color, final Shape shape) {
		this.shape = shape;
		this.color = color;
	}

	public int getColor() {
		return color;
	}

	public Shape getShape() {
		return shape;
	}

	@Override
	public <Result> Result accept(Visitor<Result> v) {
		return v.onStrokeColor(this);
	}
}




